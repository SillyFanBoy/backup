# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/Patrick/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="rkj-repos"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

#ls color

export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

#alias

alias site='ssh root@45.77.93.3'
alias tilde.town='mosh mushmouth@tilde.town'
alias cds='cd /Users/Patrick/Documents/tilde'
alias nas='cd /Volumes/M'
alias nas2='cd /Volumes/M2'
alias pirate='youtube-dl --extract-audio --audio-format mp3 --metadata-from-title "%(artist)s - %(title)s"'
alias 7z='7z x "*.7z'
alias app='cd /Applications'
alias wine86='cd /Users/Patrick/.wine/drive_c/Program\ Files\ \(x86\)'
alias editor='emacs -nw'
alias bbs='wine /Users/Patrick/Downloads/syncterm/syncterm.exe'
alias update='source ~/.zshrc'
alias joeb='joe ~/.zshrc'
alias snes='wine /Users/Patrick/Downloads/zsnesw151-761/zsnesw.exe'
alias game='cd /Volumes/M/games/weebftp.org/Games'
alias nmusic='cd /Volumes/M/Cartoons/weebftp.org/Music'
alias elder4='cd /Users/patrick/Applications/Wineskin/Oblivion.app'
alias c='clang++'
alias music='cd /Volumes/M/music'
alias snes='wine /Users/patrick/Downloads/zsnesw151-402/zsnesw.exe '
alias cdoc='cd /Users/patrick/Documents'
alias cdd='cd /Users/patrick/Downloads'
alias cc='calc'
alias editd='nano doom ~/Library/Preferences/gzdoom.ini'
alias osu='open /Volumes/M/osu/opsu-0.16.0.jar'
alias netscape='wine /Users/patrick/.wine/drive_c/Program\ Files/Netscape/Navigator/Program/netscape.exe'
alias backb='cp .zshrc zshrc'
alias upb='scp zshrc mushmouth@tilde.town:/home/mushmouth/dotfiles'
alias winsteam='wine /Users/patrick/.wine/drive_c/Program\ Files\ \(x86\)/Steam/Steam.exe'
alias midi='cd /Volumes/M/midi'
alias ls='dirr'
alias hn='haxor-news'
alias sd='wine /Volumes/M/super\ sd\ card/setupsdV271en.exe'
alias play='timidity'
alias media='cd /Volumes/M/Cartoons/weebftp.org'
alias i2p='/Applications/i2p/runplain.sh'
alias nameserver='more /etc/resolv.conf'
alias old='cd /Volumes/M/Cartoons/weebftp.org/Music/old'
alias gog='cd /Users/patrick/.wine/drive_c/GOG\ Games'
alias dropbox='cd ~/Dropbox'
alias emac='emacs -nw'
alias admin='mosh mushmouth@172.104.141.170'
alias ps2='open /Users/Patrick/Applications/Wineskin/PS2.app/Wineskin.app'
alias emu='open /Applications/OpenEmu.app'
alias code='cd /Volumes/M/code/'
alias joec='joe /usr/local/etc/joe/joerc'
alias gophert='lynx gopher://tilde.town'
alias utc='sudo ln -sf /usr/share/zoneinfo/UTC /etc/localtime'
alias edt='sudo ln -sf /usr/share/zoneinfo/EDT /etc/localtime'
alias cd-i='wine /Volumes/M/cdiemu-0.5.2/wcdiemu.exe'
alias tilde='cd /Volumes/M/code/html/tilde'
alias tilde2='cd /Volumes/tilde'
alias emacs='emacs -nw'
alias weather='sh /Users/patrick/weather'
alias irct='ssh -N mushmouth@tilde.town -L localhost:6667:localhost:6667'
alias play='TiMidity'
alias emacsi='emacs ~/.emacs.d/init.el'
alias emacsd='cd /Users/patrick/.emacs.d'
alias emacsc='emacs ~/.emacs'
alias pomo='muccadoro | tee -ai ~/pomodoros.txt'
alias t='twterm'
alias yt='mpsyt'
alias slack='slackadaisical'
alias speed='speedtest-cli --share'
alias zdl='wine /Volumes/M/iwad/ZDL3.1a/zdl.exe'
alias ytm='cd /Volumes/M/ytm'
alias videos='cd /Volumes/M/myvideos'
alias twit='rainbowstream'
alias pi='pianobar'
alias mu='cmus'
alias tf='tail -F'
alias tailf='tail -F /var/log/system.log'
alias upd='tail -F /var/log/install.log'
alias mas='tail -F /var/log/commerce.lo'
alias vgm='cd /Volumes/M/music/videogamemusic'
alias bit='mpv http://bitreich.org:3232/live'
alias 2f30='mpv https://radio.2f30.org:8443/live.mp3'
alias gopher='cgo'
alias mpv2='mpv --quiet -vo caca'
alias media2='cd /Volumes/M2/media'
alias keygen='mpv http://stream.keygen-fm.ru:8082/listen.pls'
alias gopher='elinks'
alias mushmouth.tech='ssh root@mushmouth.tech'
alias backup='cp /Users/patrick/.zshrc /Volumes/M2/backup && cp /usr/local/etc/joe/joerc /Volumes/M2/backup && cp /Users/patrick/.motd /Volumes/M2/backup' 
alias backupf='cd /Volumes/M2/backup'
#call motd
if [[ -e $HOME/.motd ]]; then cat $HOME/.motd; fi

#cmus-osx
export PYTHON_CONFIGURE_OPTS="--enable-framework"
export PATH="$HOME/.composer/vendor/bin:$PATH"
